print("This file will be run at load time!")


minetest.register_node("testmod:node", {  --registers a node (placable)
    description = "TEST",  -- this is what shows up in the tooltip as the "name"
    tiles = {"testmod_node.png"},  -- looks like you can tile faces differently
    groups = {cracky = 1}  -- stone line, mined by pickaxe
})


minetest.register_craft({
    type = "shapeless",
    output = "testmod:node 2",   -- gives you 3 of the test nodes
    recipe = { "mcl_core:dirt", "mcl_core:stone" },
})